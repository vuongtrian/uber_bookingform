function checkUberType() {
  var uberX_Input = document.getElementById("uberX");
  var uberSUC_Input = document.getElementById("uberSUV");
  var uberBlack_Input = document.getElementById("uberBlack");

  if (uberX_Input.checked) {
    return "uberX";
  } else if (uberSUC_Input.checked) {
    return "uberSUV";
  } else if (uberBlack_Input.checked) {
    return "uberBlack";
  } else {
    alert("Vui lòng chọn loại xe");
  }
}

function calTotalAmount(
  distance,
  waitingTime,
  firstPrice,
  midPrice,
  lastPrice,
  waitingPrice
) {
  if (distance <= 1) {
    return firstPrice + waitingTime * waitingPrice;
  }
  if (distance > 1 && distance <= 20) {
    return firstPrice + (distance - 1) * midPrice + waitingTime * waitingPrice;
  }
  if (distance > 20) {
    return (
      firstPrice +
      (distance - 1) * midPrice +
      (distance - 20) * lastPrice +
      waitingTime * waitingPrice
    );
  }
}

function main() {
  var distance = document.getElementById("txtDistance").value;
  var waitingTime = document.getElementById("txtWaiting").value;
  var uberType;
  var totalAmount = 0;

  distance = +distance;
  waitingTime = +waitingTime;

  uberType = checkUberType();

  switch (uberType) {
    case "uberX":
      totalAmount = calTotalAmount(
        distance,
        waitingTime,
        8000,
        12000,
        10000,
        2000
      );
      break;

    case "uberSUV":
      totalAmount = calTotalAmount(
        distance,
        waitingTime,
        9000,
        14000,
        16000,
        2000
      );
      break;

    case "uberBlack":
      totalAmount = calTotalAmount(
        distance,
        waitingTime,
        10000,
        16000,
        14000,
        4000
      );
      break;

    default:
      return;
  }

  document.getElementById("divThanhTien").style.display = "block";
  document.getElementById("xuatTien").innerHTML =
    totalAmount.toLocaleString() + " VND";
};
